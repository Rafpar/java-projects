package pl.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Time;

@Entity
public class Reservation implements Serializable{
    public static final long serialVersionUID = 5L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Time lessonhour;
    private String className;
    private String classroom;
    private String teacher;

    Reservation(){}

    public Reservation(Time lessonhour, String className, String classroom, String teacher){
        this.lessonhour = lessonhour;
        this.className = className;
        this.classroom = classroom;
        this. teacher = teacher;
    }

    public Time getLessonhour() {
        return lessonhour;
    }

    public void setLessonhour(Time lessonhour) {
        this.lessonhour = lessonhour;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
