package pl.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Time;


@Entity
public class LessonHours implements Serializable{
    public static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String lessonHour;


    LessonHours(){}

    public LessonHours(String lessonHour){
        this.lessonHour = lessonHour;
    }

    public String getLessonHour() {
        return lessonHour;
    }

    public void setLessonHour(String lessonHour) {
        this.lessonHour = lessonHour;
    }
}
