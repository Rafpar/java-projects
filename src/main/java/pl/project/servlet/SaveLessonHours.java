package pl.project.servlet;

import pl.project.dao.LessonHoursDao;
import pl.project.model.LessonHours;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;

@WebServlet("/saveLessonhour")
public class SaveLessonHours extends HttpServlet{
    public static final long serialVersionUID = 3L;

    @Inject
    LessonHoursDao lessonHoursDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws SecurityException, IOException{
        String lessonHour = request.getParameter("lessonHour");

        LessonHours lessonH = new LessonHours(lessonHour);
        lessonHoursDao.save(lessonH);
        response.sendRedirect(request.getContextPath());
    }



}
