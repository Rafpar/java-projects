package pl.project.servlet;

import pl.project.dao.ClassesDao;
import pl.project.model.Classes;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/save")
public class SaveClasses extends HttpServlet{
    private static final long serialVersionUID = 1L;

    @Inject
    private ClassesDao classesDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws SecurityException, IOException{
        String className = request.getParameter("className");
        Classes newClass = new Classes(className);
        classesDao.save(newClass);
        response.sendRedirect(request.getContextPath());
    }
}
