package pl.project.servlet;


import pl.project.dao.ClassroomsDao;
import pl.project.model.Classrooms;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/saveClassroom")
public class SaveClassrooms extends HttpServlet{
    public static final long serialVersionUID = 2L;

    @Inject
    ClassroomsDao classroomsDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws SecurityException, IOException{
        String classroom = request.getParameter("classroom");
        Classrooms newClassroom = new Classrooms(classroom);
        classroomsDao.save(newClassroom);
        response.sendRedirect(request.getContextPath());

    }
}
