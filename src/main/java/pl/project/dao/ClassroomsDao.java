package pl.project.dao;


import pl.project.model.Classrooms;

public interface ClassroomsDao {
    public void save(Classrooms classroom);
}
