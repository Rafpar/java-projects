package pl.project.dao;


import pl.project.model.Classes;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

@RequestScoped
public class ClassesDaoImpl implements ClassesDao{

    @PersistenceUnit(name = "myPersistenceUnit")
    private EntityManagerFactory emFactory;


    @Override
    public void save(Classes className) {
        EntityManager entityManager = emFactory.createEntityManager();
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(className);
        tx.commit();
        entityManager.close();
    }
}
