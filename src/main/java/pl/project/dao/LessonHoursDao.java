package pl.project.dao;


import pl.project.model.LessonHours;

public interface LessonHoursDao {
    public void save(LessonHours lessonHour);
}
