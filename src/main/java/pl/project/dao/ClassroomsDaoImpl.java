package pl.project.dao;


import pl.project.model.Classrooms;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

@RequestScoped
public class ClassroomsDaoImpl implements ClassroomsDao{

    @PersistenceUnit(name = "myPersistenceUnit")
    private EntityManagerFactory emFactory;


    @Override
    public void save(Classrooms classroom) {
        EntityManager entityManager = emFactory.createEntityManager();
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(classroom);
        tx.commit();
        entityManager.close();
    }
}

