package pl.project.dao;


import pl.project.model.Reservation;

public interface ReservationDao {
    public void save(Reservation reserve);
}
