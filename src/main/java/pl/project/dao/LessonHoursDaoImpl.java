package pl.project.dao;

import pl.project.model.LessonHours;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

@RequestScoped
public class LessonHoursDaoImpl implements LessonHoursDao{

    @PersistenceUnit(name = "myPersistenceUnit")
    private EntityManagerFactory emFactory;

    @Override
    public void save(LessonHours lessonhour){
        EntityManager entityManager = emFactory.createEntityManager();
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(lessonhour);
        tx.commit();
        entityManager.close();
    }
}